'use strict';

const jsonld = require('jsonld'),
	{ parseRDF } = require('../common'),
	{ cond, matches, merge, isEmpty, head/*, isPlainObject*/, partition } = require('lodash');
	// deepForEach = require('deep-for-each');

const ealNS = 'http://tech4comp/eal/';
const itemContextGeneral = {
	// '@language': 'en',
	title: 'http://purl.org/dc/terms/title',
	description: ealNS + 'context',
	task: ealNS + 'task',
	images: {'@id': 'http://schema.org/image', '@container': '@set'},
	difficulty: {'@id': ealNS + 'difficulty', '@type': 'http://www.w3.org/2001/XMLSchema#integer'},//ealNS + 'difficulty'
	expectedSolvability: {'@id': ealNS + 'expectedSolvability', '@type': 'http://www.w3.org/2001/XMLSchema#double'},//ealNS + 'expectedSolvability'
	performanceLevel: { '@id': ealNS + 'hasPerformanceLevel', '@type': '@id' },
	knowledgeDimension: { '@id': ealNS + 'hasKnowledgeDimension', '@type': '@id' },
	learningOutcome: { '@id': ealNS + 'hasLearningOutcome', '@type': '@id' },
	annotations: {'@id': ealNS + 'annotation'},
	flagged: {'@id': ealNS + 'flagged', '@type': 'http://www.w3.org/2001/XMLSchema#boolean'},//ealNS + 'flagged',
	note: 'http://www.w3.org/2004/02/skos/core#note',
	bloom: {'@id': ealNS + 'hasBloomMapping', '@container': '@set'},
	points: {'@id': ealNS + 'points', '@type': 'http://www.w3.org/2001/XMLSchema#decimal'},//ealNS + 'points',
	status: ealNS + 'status'
};

const singeChoiceItemContext = {
	answers: {'@id': ealNS + 'hasAnswer', '@container': '@list'},
	text: {'@id': ealNS + 'answerText'}
};

const multipleChoiceItemContext = {
	pointsNotSelected: {'@id': ealNS + 'pointsNotSelected', '@type': 'http://www.w3.org/2001/XMLSchema#decimal'},//ealNS + 'pointsNotSelected'
	maximumSelectableAnswers: {'@id': ealNS + 'maximumSelectableAnswers', '@type': 'http://www.w3.org/2001/XMLSchema#integer'},//ealNS + 'maximumSelectableAnswers'
	minimumSelectableAnswers: {'@id': ealNS + 'minimumSelectableAnswers', '@type': 'http://www.w3.org/2001/XMLSchema#integer'}//ealNS + 'minimumSelectableAnswers'
};

const projectContext = {
	// '@language': 'en',
	title: 'http://purl.org/dc/terms/title',
	description: 'http://purl.org/dc/terms/description',
	authorizedUsers: {'@id': 'http://dataid.dbpedia.org/ns/core#authorizedAgent', '@container': '@set', '@type': '@id' },
};

const sharedContext = {
	project: { '@id': ealNS + 'hasProject', '@type': '@id' },
	contributors: {'@id': 'http://purl.org/dc/terms/contributor', '@container': '@set', '@type': '@id' },
	topics: {'@id': 'http://purl.org/dc/terms/subject', '@container': '@set', '@type': '@id' }
};

const learningOutcomeContext = {
	bloom: {'@id': ealNS + 'hasBloomMapping', '@container': '@set'},
	performanceLevel: { '@id': ealNS + 'hasPerformanceLevel', '@type': '@id' },
	knowledgeDimension: { '@id': ealNS + 'hasKnowledgeDimension', '@type': '@id' },
};

const otherContext = {
	title: 'http://purl.org/dc/terms/title',
	description: 'http://purl.org/dc/terms/description',
	source: 'http://purl.org/dc/terms/source',
	abbreviation: 'http://dbpedia.org/ontology/abbreviation',
	sameAs: 'http://www.w3.org/2002/07/owl#sameAs'
};

const timeContext = {
	modificationTime: 'http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#dateModified',
	creationTime: 'http://purl.org/healthcarevocab/v1#CreationTime',
};

const remoteItemContext = {
	remoteItemURL: ealNS + 'remoteItemURL'
};

const arrangementItemContext = {
	sequenceStep: {'@id': ealNS + 'sequenceStep', '@type': 'http://www.w3.org/2001/XMLSchema#integer'}
};

module.exports = {

	addContext: async function(data, subject, type) {
		data['@id'] = subject;
		data['@type'] = type.replace('>','').replace('<','');
		data['@context'] = getContextFor(type);
		if(data.answers) {
			data.answers.forEach((answer) => {
				answer['@type'] = ealNS + 'Answer';
			});
		}
		return data;
	},

	ntriplesToJSONLD: async function(ntriples, type = '') {
		let time1 = new Date().getTime();
		const toCompact = await jsonld.fromRDF(ntriples);
		applyDataFormatFixes(toCompact);
		let time2 = new Date().getTime();
		let compacted = await jsonld.compact(toCompact, getContextFor(type), {/*'compactArrays':false,*/ 'processingMode': 'json-ld-1.1', 'produceGeneralizedRdf': true});
		let time3 = new Date().getTime();
		compacted = replaceBlankNodes(compacted);
		let time4 = new Date().getTime();
		console.log('fromRDf: ' + (time2 - time1),'compact: ' + (time3 - time2),'blankNodes: ' + (time4 - time3));
		return compacted;
	},

	replaceJSONLDContext: async function(json, type = '') {
		let time1 = new Date().getTime();
		// const expanded = await jsonld.expand(json);
		const expanded = json;
		let time2 = new Date().getTime();
		let compacted = await jsonld.compact(expanded, getContextFor(type), {/*'compactArrays':false,*/ 'processingMode': 'json-ld-1.1'});
		let time3 = new Date().getTime();
		compacted = replaceBlankNodes(compacted);
		let time4 = new Date().getTime();
		console.log('expand: ' + (time2 - time1),'compact: ' + (time3 - time2),'blankNodes: ' + (time4 - time3));
		return compacted;
	},

	jsonldToQuadStrings: async function(doc) {
		const rdf = await jsonld.toRDF(doc, {format: 'application/n-quads'});
		return await parseRDF(rdf);
	}
};

function applyDataFormatFixes(expanded) {
	expanded.forEach((x) => {
		(x['http://tech4comp/eal/points'] || []).forEach((y) => {
			y['@type'] = 'http://www.w3.org/2001/XMLSchema#decimal';
		});
		(x['http://tech4comp/eal/pointsNotSelected'] || []).forEach((y) => {
			y['@type'] = 'http://www.w3.org/2001/XMLSchema#decimal';
		});
	});
}

function getContextFor(type) {
	const getContextFor = cond([//Note each pair is like switch case with pattern matching (like in scala)
		[(x) => matches('<' + ealNS + 'Project' + '>')(x) || matches('<' + ealNS + 'Topic' + '>')(x), () => {
			return merge({}, timeContext, projectContext);
		}],
		[(x) => matches('<' + ealNS + 'LearningOutcome' + '>')(x), () => {
			return merge({}, timeContext, projectContext, sharedContext, learningOutcomeContext);
		}],
		[matches('<' + ealNS + 'SingleChoiceItem' + '>'), () => {
			return merge({}, timeContext, itemContextGeneral, sharedContext, singeChoiceItemContext);
		}],
		[matches('<' + ealNS + 'MultipleChoiceItem' + '>'), () => {
			return merge({}, timeContext, itemContextGeneral, sharedContext, singeChoiceItemContext, multipleChoiceItemContext);
		}],
		[matches('<' + ealNS + 'FreeTextItem' + '>'), () => {
			return merge({}, timeContext, itemContextGeneral, sharedContext);
		}],
		[matches('<' + ealNS + 'RemoteItem' + '>'), () => {
			return merge({}, timeContext, itemContextGeneral, sharedContext, remoteItemContext);
		}],
		[matches('<' + ealNS + 'ArrangementItem' + '>'), () => {
			return merge({}, timeContext, itemContextGeneral, sharedContext, singeChoiceItemContext, arrangementItemContext);
		}],
		[matches('<' + ealNS + 'Item' + '>'), () => {
			return merge({}, timeContext, itemContextGeneral, sharedContext, singeChoiceItemContext, multipleChoiceItemContext, remoteItemContext, arrangementItemContext);
		}],
		[() => true, () => {//default match
			return merge({}, timeContext, otherContext);
		}],
	]);
	return getContextFor(type);
}

function replaceBlankNodes (compacted) {//TODO implement general blank node replacement (not just for answers)
	try {
		if(!isEmpty(compacted['@graph'])) {
			let [dataGraph, blankNodes] = partition(compacted['@graph'], (a) => !a['@id'].startsWith('_:'));
			if(!isEmpty(dataGraph) && !isEmpty(blankNodes)) {
				dataGraph = dataGraph.map((data) => {
					// TODO probably a better method, but isn't working yet because e.g. data['annoations.bloom[0]'] isn't working
					// deepForEach(data, (value, key, subject, path) => {
					// 	console.log('');
					// 	if(isPlainObject(value) && Object.keys(value).length === 1 && !isEmpty(value['@id'])) {
					// 		console.log(value, key, path);
					// 		data[path] = getBlankNode(blankNodes, value['@id']);
					// 	}
					// });
					if(!isEmpty(data.answers)) {
						data.answers.forEach((answer, index) => {
							[data.answers[index], blankNodes] = getBlankNode(blankNodes, answer['@id']);
						});
					}
					if(!isEmpty(data.annotations)) {
						[data.annotations, blankNodes] = getBlankNode(blankNodes, data.annotations['@id']);
					}
					if(!isEmpty(data.annotations) && !isEmpty(data.annotations.bloom)) {
						data.annotations.bloom.forEach((bloom, index) => {
							[data.annotations.bloom[index], blankNodes] = getBlankNode(blankNodes, bloom['@id']);
						});
					}
					if(!isEmpty(data.bloom)) {
						data.bloom.forEach((bloom, index) => {
							[data.bloom[index], blankNodes] = getBlankNode(blankNodes, bloom['@id']);
						});
					}
					return data;
				});
				if(dataGraph.length === 1)
					return Object.assign({'@context': compacted['@context']}, head(dataGraph) );
				else
					return {'@context': compacted['@context'], '@graph': dataGraph };
			}
			return compacted;
		} else
			return compacted;
	} catch (e) {
		console.warn(e);
		return compacted;
	}
}

function getBlankNode(blankNodesArray, id) {
	if(!isEmpty(blankNodesArray)) {
		let node;
		[node, blankNodesArray] = partition(blankNodesArray, (graph) => graph['@id'] === id);
		if(!isEmpty(node)) {
			return [head(node), blankNodesArray];
		} else
			throw 'Can not find a matching blank node';
	} else
		throw 'No blank nodes';
}
