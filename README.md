# EAs.LiT Item-Service #

This service provides a Linked Data and REST-API for items, learning outcomes and topics, stored in a RDF graph store. It allows to manipulate data based on the Turtle RDF format, as well as plain JSON and JSON-LD. JSON and JSON-LD were tested best, as these formats are used from other services and and the [EAs.LiT frontend](https://gitlab.com/Tech4Comp/eas.lit-ui).

The service was tested with the following Graph Stores:
* [Jena Fuseki](https://jena.apache.org/documentation/fuseki2/)
* [QuitStore](https://github.com/aksw/quitstore) ([docker image](https://hub.docker.com/r/aksw/quitstore/), tag [2019-01-02](https://github.com/AKSW/QuitStore/releases/tag/2019-01-02), note issue #5)

Nevertheless, the service should be able work with every RDF based graph store that supports quads and JSON-LD results.

## How to get started
1. Clone the repository and all sub repositories via `git clone --recursive ...`.
2. Change into the directory and install all dependencies via `npm install`
3. Transpile and run the project via `npm start`. The started service is accessible via [http://localhost:3000](http://localhost:300)

The service is now started in development mode. You may run `npm run lint` to lint all code via ESLint. Have a look at the package.json file for other available commands.

The service is configured to use a Graph Store at `http://localhost:8080/sparql`. This default value may be changed by using the environment variable `SPARQL_ENDPOINT`, e.g. `export SPARQL_ENDPOINT=http://ex.org/sparql`. For further environment variables, see the file [docker-compose.yml](./docker-compose.yml).

As soon as the service is started, an OpenAPI documentation page (swagger) is created at `/documentation` that may be used to test and experiment with this service. Test data can be found in [tests/data/](./tests/data/).

## Production Environment
Use the included dockerfile to build a docker image and the included docker-compose.yml files to start it up.

Look at the dockerfile and docker-compose.yml files to gain insights into manual deployment.

## Automatic Builds and Deployments
By including `[build]` to a commit message on the main branch, a GitLab CI pipeline is triggered and builds a docker image, which is published on GitLab. Afterwards, a deployment stage is triggered, which deploys this newly built image to the staging environment. Deployment may also be triggered on its own, by including `[deploy]` to a commit message.

## Development
The root directory contains all source code, as well as all configuration files. The easiest way to get around is to look at the routes.js file.

## Notes

To properly install all the dependencies this service needs a working version of python3, make, git, a c++ compiler (e.g. via gcc-c++/build-essential) and RaptorUtils (or raptor2).
