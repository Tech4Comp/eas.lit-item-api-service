'use strict';

const Joi = require('joi'),
	{ badRequest } = require('@hapi/boom'),
	handlers = require('./controllers/handler'),
	{ isEmpty } = require('lodash'),
	{ hostname } = require('./config');

const bloom = Joi.array().items(
	Joi.object().keys({
		performanceLevel: Joi.string().trim().uri().example('http://tech4comp/eal/understand'),
		knowledgeDimension: Joi.string().trim().uri().example('http://tech4comp/eal/metacognitive'),
	}).fork(['performanceLevel', 'knowledgeDimension'], (schema) => schema.required()).label('bloom')
).label('bloom array');

const idAsteriskParam = Joi.object({
	id: Joi.string().trim().lowercase().description('Use * to request a list of all existing entities')
});

const idParam = Joi.object({
	id: Joi.string().trim().alphanum().lowercase()
});

const answers = Joi.array().items(
	Joi.object().keys({
		text: Joi.string().trim().example('Correct Answer'),
		points: Joi.number().min(0).example(3),
	}).fork(['text', 'points'], (schema) => schema.required()).label('simple answer')
).min(2);

const answersArrangement = Joi.array().items(
	Joi.object().keys({
		text: Joi.string().trim().example('Correct Answer'),
		points: Joi.number().min(0).example(3),
		sequenceStep: Joi.number().integer().min(1).example(1),
	}).fork(['text', 'points', 'sequenceStep'], (schema) => schema.required()).label('arrangement answer')
).min(2);

const answersMC = Joi.array().items(
	Joi.object().keys({
		text: Joi.string().trim().example('Correct Answer'),
		points: Joi.number().min(0).example(3),
		pointsNotSelected: Joi.number().min(0).example(0),
	}).fork(['text', 'points', 'pointsNotSelected'], (schema) => schema.required()).label('multiple choice answer')
).min(2);

const annotations = Joi.object().keys({
	flagged: Joi.boolean(),
	status: Joi.string().trim().allow('draft', 'published', 'revisioned', 'toRevise', 'deferred').example('draft').default('draft'),
	note: Joi.string().trim().example('Überarbeiten'),
	topics: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
	bloom: bloom
}).label('annotations');

module.exports = [

	{
		method: 'GET',
		path: '/performancelevel/{name}',
		config: {
			handler: handlers.getDefaultData,
			tags: ['api', 'list', 'a <http://tech4comp/eal/PerformanceLevel>'],
			description: 'Get the specified performance level item',
			validate: {
				params: Joi.object({
					name: Joi.string().trim().alphanum().lowercase().valid('remember', 'understand', 'apply', 'analyze', 'evaluate', 'create', '*').description('Use * to request a list of all existing entities')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/knowledgedimension/{name}',
		config: {
			handler: handlers.getDefaultData,
			tags: ['api', 'list', 'a <http://tech4comp/eal/KnowledgeDimension>'],
			description: 'Get the specified knowledge dimension item',
			validate: {
				params: Joi.object({
					name: Joi.string().trim().alphanum().lowercase().valid('factual', 'conceptual', 'procedural', 'metacognitive', '*').description('Use * to request a list of all existing entities')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/item/{id}',
		config: {
			handler: handlers.getSCMCItem,
			tags: ['api', 'a <http://tech4comp/eal/Item>', 'a <http://tech4comp/eal/SingleChoiceItem>', 'a <http://tech4comp/eal/MultipleChoiceItem>', 'a <http://tech4comp/eal/FreeTextItem>', 'a <http://tech4comp/eal/RemoteItem>', 'a <http://tech4comp/eal/ArrangementItem>'],
			description: 'Get an item',
			validate: {
				params: idAsteriskParam,
				query: Joi.object({
					project: Joi.string().trim().uri().description('Use a project URI together with id: * to filter the resulting list by project'),
					detailed: Joi.boolean().description('Use together with id: * to get items with all properties')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/item/singleChoice/{id}',
		config: {
			handler: handlers.getSCMCItem,
			tags: ['api', 'a <http://tech4comp/eal/SingleChoiceItem>'],
			description: 'Get an item',
			validate: {
				params: idAsteriskParam
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/item/multipleChoice/{id}',
		config: {
			handler: handlers.getSCMCItem,
			tags: ['api', 'a <http://tech4comp/eal/MultipleChoiceItem>'],
			description: 'Get an item',
			validate: {
				params: idAsteriskParam
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/item/freeText/{id}',
		config: {
			handler: handlers.getSCMCItem,
			tags: ['api', 'a <http://tech4comp/eal/FreeTextItem>'],
			description: 'Get an item',
			validate: {
				params: idAsteriskParam
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/item/arrangement/{id}',
		config: {
			handler: handlers.getSCMCItem,
			tags: ['api', 'a <http://tech4comp/eal/ArrangementItem>'],
			description: 'Get an item',
			validate: {
				params: idAsteriskParam
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/item/remote/{id}',
		config: {
			handler: handlers.getSCMCItem,
			tags: ['api', 'a <http://tech4comp/eal/RemoteItem>'],
			description: 'Get a remote item',
			validate: {
				params: Joi.object({
					id: Joi.string().trim().lowercase().description('Use * to request a list of all existing entities or use a remoteItemURL to search for the specific item')
				}),
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/item/singleChoice',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/SingleChoiceItem>'],
			description: 'Create a single choice item',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					creationTime: Joi.forbidden(),
					modificationTime: Joi.forbidden(),
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					answers: answers,
					annotations: annotations
				}).fork(['task', 'contributors', 'answers'], (schema) => schema.required()).label('single choice post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/item/singleChoice/{id}',
		config: {
			handler: handlers.update,
			tags: ['api', 'a <http://tech4comp/eal/SingleChoiceItem>'],
			description: 'Update an existing single choice item',
			validate: {
				options: {allowUnknown: true},
				params: idParam,
				payload: Joi.object().keys({
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					answers: answers,
					annotations: annotations,
					creationTime: Joi.date().greater('1-1-2019').iso(),
					modificationTime: Joi.date().greater('1-1-2019').iso()
				}).fork(['task', 'contributors', 'answers'], (schema) => schema.required()).label('single choice put'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/item/multipleChoice',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/MultipleChoiceItem>'],
			description: 'Create a multiple choice item',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					minimumSelectableAnswers: Joi.number().integer().positive().min(0).example(1),
					maximumSelectableAnswers: Joi.number().integer().positive().min(1).example(2),
					answers: answersMC,
					annotations: annotations
				}).fork(['task', 'contributors', 'answers', 'maximumSelectableAnswers', 'minimumSelectableAnswers'], (schema) => schema.required()).label('multiple choice post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/item/multipleChoice/{id}',
		config: {
			handler: handlers.update,
			tags: ['api', 'a <http://tech4comp/eal/MultipleChoiceItem>'],
			description: 'Update an existing multiple choice item',
			validate: {
				options: {allowUnknown: true},
				params: idParam,
				payload: Joi.object().keys({
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					minimumSelectableAnswers: Joi.number().integer().positive().min(0).example(1),
					maximumSelectableAnswers: Joi.number().integer().positive().min(1).example(2),
					answers: answersMC,
					annotations: annotations,
					creationTime: Joi.date().greater('1-1-2019').iso(),
					modificationTime: Joi.date().greater('1-1-2019').iso()
				}).fork(['task', 'contributors', 'answers', 'maximumSelectableAnswers', 'minimumSelectableAnswers'], (schema) => schema.required()).label('multiple choice put'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/item/freeText',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/FreeTextItem>'],
			description: 'Create a free text item',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					points: Joi.number().min(0).example(3),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					annotations: annotations
				}).fork(['task', 'contributors', 'points'], (schema) => schema.required()).label('free text post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/item/freeText/{id}',
		config: {
			handler: handlers.update,
			tags: ['api', 'a <http://tech4comp/eal/FreeTextItem>'],
			description: 'Update an existing free text item',
			validate: {
				options: {allowUnknown: true},
				params: idParam,
				payload: Joi.object().keys({
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					points: Joi.number().min(0).example(3),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					annotations: annotations,
					creationTime: Joi.date().greater('1-1-2019').iso(),
					modificationTime: Joi.date().greater('1-1-2019').iso()
				}).fork(['task', 'contributors', 'points'], (schema) => schema.required()).label('free text put'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/item/arrangement',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/ArrangementItem>'],
			description: 'Create a arrangement item',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					answers: answersArrangement,
					annotations: annotations
				}).fork(['task', 'contributors', 'answers'], (schema) => schema.required()).label('arrangement post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/item/arrangement/{id}',
		config: {
			handler: handlers.update,
			tags: ['api', 'a <http://tech4comp/eal/ArrangementItem>'],
			description: 'Update an existing arrangement item',
			validate: {
				options: {allowUnknown: true},
				params: idParam,
				payload: Joi.object().keys({
					title: Joi.string().trim().example('The answer to everything'),
					description: Joi.string().trim().example('A hitchhikers guide to the galaxy'),
					task: Joi.string().trim().example('What is the answer to everything?'),
					images: Joi.array().items(Joi.string().trim().uri().example('http://ex.org/image/17.jpg')),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					answers: answersArrangement,
					annotations: annotations,
					creationTime: Joi.date().greater('1-1-2019').iso(),
					modificationTime: Joi.date().greater('1-1-2019').iso()
				}).fork(['task', 'contributors', 'answers'], (schema) => schema.required()).label('arrangement put'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/item/remote',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/RemoteItem>'],
			description: 'Create a remote item',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					remoteItemURL: Joi.string().trim().uri().example('http://onyx/item/25'),
					title: Joi.string().trim().example('The answer to everything'),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					annotations: annotations
				}).fork(['remoteItemURL', 'title', 'contributors'], (schema) => schema.required()).label('remote post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/item/remote/{id}',
		config: {
			handler: handlers.update,
			tags: ['api', 'a <http://tech4comp/eal/RemoteItem>'],
			description: 'Update an existing remote item',
			validate: {
				options: {allowUnknown: true},
				params: idParam,
				payload: Joi.object().keys({
					remoteItemURL: Joi.string().trim().uri().example('http://onyx/item/25'),
					title: Joi.string().trim().example('The answer to everything'),
					difficulty: Joi.number().min(0).max(100).example(10).default(50),
					expectedSolvability: Joi.number().positive().max(1).precision(1).example(0.2).default(0.5),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					learningOutcome: Joi.string().trim().uri().example('http://localhost:3000/learningoutcome/20'),
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					annotations: annotations,
					creationTime: Joi.date().greater('1-1-2019').iso(),
					modificationTime: Joi.date().greater('1-1-2019').iso()
				}).fork(['remoteItemURL', 'title', 'contributors'], (schema) => schema.required()).label('remote put'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'DELETE',
		path: '/item/{id}',
		config: {
			handler: handlers.delete,
			tags: ['api'],
			description: 'Delete an item',
			validate: {
				params: idParam
			},
			plugins: {
				'hapi-swagger': {
					responses: {
						400: {
							description: 'Probably a parameter is missing or not allowed'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/project/{id}',
		config: {
			handler: handlers.get,
			tags: ['api', 'a <http://tech4comp/eal/Project>'],
			description: 'Get a Project',
			validate: {
				params: idAsteriskParam
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/project',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/Project>'],
			description: 'Create a Project',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					title: Joi.string().trim(),
					description: Joi.string().trim(),
					authorizedUsers: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1)
				}).fork(['title', 'description', 'authorizedUsers'], (schema) => schema.required()).label('project post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/project/{id}',
		config: {
			handler: handlers.update,
			tags: ['api', 'a <http://tech4comp/eal/Project>'],
			description: 'Update an existing Project',
			validate: {
				options: {allowUnknown: true},
				params: idParam,
				payload: Joi.object().keys({
					title: Joi.string().trim(),
					description: Joi.string().trim(),
					authorizedUsers: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					creationTime: Joi.date().greater('1-1-2019').iso(),
					modificationTime: Joi.date().greater('1-1-2019').iso()
				}).fork(['title', 'description', 'authorizedUsers'], (schema) => schema.required()).label('project put'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'DELETE',
		path: '/project/{id}',
		config: {
			handler: handlers.delete,
			tags: ['api'],
			description: 'Delete a Project',
			validate: {
				params: idParam
			},
			plugins: {
				'hapi-swagger': {
					responses: {
						400: {
							description: 'Probably a parameter is missing or not allowed'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/topic/{id}',
		config: {
			handler: handlers.get,
			tags: ['api', 'a <http://tech4comp/eal/Topic>'],
			description: 'Get a Topic',
			validate: {
				params: idAsteriskParam
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/topic',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/Topic>'],
			description: 'Create a Topic',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					title: Joi.string().trim()
				}).fork(['title'], (schema) => schema.required()).label('topic post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'DELETE',
		path: '/topic/{id}',
		config: {
			handler: handlers.delete,
			tags: ['api'],
			description: 'Delete a topic',
			validate: {
				params: idParam
			},
			plugins: {
				'hapi-swagger': {
					responses: {
						400: {
							description: 'Probably a parameter is missing or not allowed'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/learningoutcome/{id}',
		config: {
			handler: handlers.get,
			tags: ['api', 'a <http://tech4comp/eal/LearningOutcome>'],
			description: 'Get a learning outcome',
			validate: {
				params: idAsteriskParam,
				query: Joi.object({
					project: Joi.string().trim().uri().description('Use a project URI together with id: * to filter the resulting list by project')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json', 'application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/learningoutcome',
		config: {
			handler: handlers.create,
			tags: ['api', 'a <http://tech4comp/eal/LearningOutcome>'],
			description: 'Create a learning outcome',
			validate: {
				options: {allowUnknown: true},
				payload: Joi.object().keys({
					'@id': Joi.forbidden(),
					title: Joi.string().trim(),
					description: Joi.string().trim(),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					bloom: bloom.min(1),
					topics: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1)
				}).fork(['title', 'description', 'contributors', 'bloom', 'topics'], (schema) => schema.required()).label('learning outcome post'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/learningoutcome/{id}',
		config: {
			handler: handlers.update,
			tags: ['api', 'a <http://tech4comp/eal/LearningOutcome>'],
			description: 'Update an existing learning outcome',
			validate: {
				options: {allowUnknown: true},
				params: idParam,
				payload: Joi.object().keys({
					title: Joi.string().trim(),
					description: Joi.string().trim(),
					project: Joi.string().trim().uri().default( hostname + '/project/1').description('URI to a created project'), // TODO Correct default value?!
					contributors: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					bloom: bloom.min(1),
					topics: Joi.array().items(Joi.string().trim().uri().example('http://example.org/me')).min(1),
					creationTime: Joi.date().greater('1-1-2019').iso(),
					modificationTime: Joi.date().greater('1-1-2019').iso()
				}).fork(['title', 'description', 'contributors', 'bloom', 'topics'], (schema) => schema.required()).label('learning outcome put'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json','application/ld+json', 'text/turtle'],
					produces: ['application/json','application/ld+json', 'text/turtle'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'DELETE',
		path: '/learningoutcome/{id}',
		config: {
			handler: handlers.delete,
			tags: ['api'],
			description: 'Delete a learning outcome',
			validate: {
				params: idParam
			},
			plugins: {
				'hapi-swagger': {
					responses: {
						400: {
							description: 'Probably a parameter is missing or not allowed'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/onyx/{hash}',
		config: {
			handler: handlers.getOnyxID,
			tags: ['api'],
			description: 'Get an ID of a Onyx Item via its SHA224 hash',
			validate: {
				params: Joi.object({
					hash: Joi.string().trim().alphanum().length(56).description('SHA224 hash of the item zip file')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						}
					}
				}
			}
		}
	},

];

function failAction(request, h, err) {
	const type = request.headers['content-type'];
	if(type !== 'application/json' && type !== 'application/ld+json')
		if(isEmpty(request.payload))
			return badRequest('No data');
		else
			return h.continue;
	else
		throw badRequest(err.output.payload.message);
}
