'use strict';

const hapi = require('@hapi/hapi'),
	{ isTurtle, parseRDF } = require('./common'),
	{ isEmpty } = require('lodash'),
	sparql = require('./sparql'),
	{ readFileSync } = require('fs'),
	{ resolve } = require('path'),
	config = require('./config'),
	{ loadShapes } = require('./shacl-validation/shacl');

global.appRoot = resolve(__dirname);

const server = new hapi.Server({
	port: config.port,
	routes: {cors: true}
});

module.exports = server;
global.server = server;

server.ext('onPreResponse', (request, h) => {
	const response = request.response;
	if (response.isBoom) {
		const is4xx = response.output.statusCode >= 400 && response.output.statusCode < 500;
		if (is4xx && response.data)
			response.output.payload.data = response.data;
	}
	return h.continue;
});

const plugins = [
	require('@hapi/inert'),
	require('@hapi/vision'), {
		plugin: require('@hapi/good'),
		options: {
			ops: {
				interval: 1000
			},
			reporters: {
				console: [{
					module: '@hapi/good-squeeze',
					name: 'Squeeze',
					args: [{
						log: '*',
						response: '*',
						request: '*'
					}]
				}, {
					module: '@hapi/good-console'
				}, 'stdout']
			}
		}
	}, {
		plugin: require('hapi-swagger'),
		options: {
			host: config.domain + ((config.port !== '80') ? ':' + config.port : ''),
			info: {
				title: 'Item API',
				description: 'Powered by node, hapi, joi, hapi-swaggered, hapi-swaggered-ui and swagger-ui',
				version: '0.1.0'
			}
		}
	}
];

async function init() {
	await checkDataPresent('http://tech4comp/eal/remember', __dirname + '/eal-vocabulary-and-shapes/default-data.ttl', config.dataGraphName);
	await checkDataPresent('http://tech4comp/eal/titleShape', __dirname + '/eal-vocabulary-and-shapes/shapes.ttl', config.shapesGraphName);
	await loadShapes();
	await checkDataPresent('http://tech4comp/eal/Answer', __dirname + '/eal-vocabulary-and-shapes/concepts-and-properties.ttl', config.conceptsGraphName);
	await sparql.createCounters();
	await server.register(plugins);
	server.route(require('./routes.js'));

	await server.start();
	server.log('info', 'Server started at ' + server.info.uri);

}

process.on('unhandledRejection', (err) => {
	console.error(err);
	process.exit(1);
});

async function checkDataPresent(subjectToTest, filenameToLoad, graphName) {
	//TODO Request all data from graph and deep compare to file content. On mismatch, clear graph and reload from files???
	//IDEA juse jsonld.canonize to get a comparable string
	try {
		let result = undefined;
		try {
			result = await sparql.read(subjectToTest, undefined, 'application/n-triples', graphName);
		} catch (e) {}//eslint-disable-line
		if (isEmpty(result)){
			const data = readFileSync(filenameToLoad).toString();
			if(await isTurtle(data)){
				const toWrite = await parseRDF(data);
				await sparql.write(toWrite, graphName, true);
			} else {
				throw filenameToLoad + ' contains errors, please check with RaptorUtils';
			}
		}	else {
			console.info('Data for graph ' + graphName + ' is already present. Graph content has not been overwritten.');
		}
	} catch (e) {
		console.error('Unable to load data', e);
		process.exit(1);
	}
}

init();
