'use strict';

const SHACLValidator = require('shacl-js'),
	{ sparqlEndpoint, shapesGraphName } = require('../config'),
	{ parseRDF } = require('../common'),
	// fs = require('fs'),
	{ post } = require('got');

let shapes = undefined;

module.exports = {

	validate: async function(data) {

		// shapes = fs.readFileSync('./data-concepts-shapes/shapes.ttl').toString();
		// console.log(shapes);

		return new Promise((resolve, reject) => {
			let validator = new SHACLValidator();
			validator.validate(data.toString(), 'text/turtle', shapes, 'text/turtle', (e, report) => {
				if(report.conforms() === true)
					resolve(true);
				else
					reject({'@type': 'ValidationReport', results: report.results()});
			});
		});
	},

	loadShapes: async function() {
		let response = await post(sparqlEndpoint, {
			method: 'POST',
			headers: {'Content-Type': 'application/sparql-query', 'Accept': 'text/turtle'},
			body: `CONSTRUCT { ?s ?p ?o .	} WHERE { GRAPH <${shapesGraphName}> { ?s ?p ?o . } }`
		});
		response = response.body;

		if((await parseRDF(response)).length === 0)
			throw 'Error loading shapes from store: ' + response;
		shapes = response;
		console.info('Successfully loaded shapes from store');
		return true;
	}

};
