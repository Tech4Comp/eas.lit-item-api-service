/* eslint dot-notation: 0, no-unused-vars: 0 */
'use strict';

describe('REST API', () => {

	let server;

	beforeEach((done) => {
		Object.keys(require.cache).forEach((key) => delete require.cache[key]);
		require('chai').should();
		let hapi = require('@hapi/hapi');
		server = hapi.Server({
			host: 'localhost',
			port: 3000
		});
		server.route(require('../routes.js'));
		done();
	});

	let options = {
		method: 'GET',
		url: '/item/1',
		headers: {
			'Content-Type': 'application/json'
		}
	};

	context('when creating an item it', () => {
		it('should reply it', async () => {
			let response = await server.inject(options);
			response.should.be.an('object').and.contain.keys('statusCode','payload');
			response.statusCode.should.equal(200);
			response.payload.should.be.a('string');
			let payload = JSON.parse(response.payload);
			payload.should.be.an('object').and.contain.keys('title', 'language');
			payload.title.should.equal('Dummy');
			payload.language.should.equal('en');
		});
	});
});
