/* eslint dot-notation: 0, no-unused-vars: 0 */
'use strict';

describe('REST API', () => {
	// TODO Not working yet!

	let server;

	beforeEach((done) => {
		Object.keys(require.cache).forEach((key) => delete require.cache[key]);
		require('chai').should();
		let hapi = require('@hapi/hapi');
		server = hapi.Server({
			host: 'localhost',
			port: 3000
		});
		server.route(require('../routes.js'));
		done();
	});

	let options0 = {
		method: 'POST',
		url: '/project',
		headers: {
			'Content-Type': 'application/json'
		},
		body: {
			title: 'First',
			description: 'test',
		}
	};
	let options1 = {
		method: 'GET',
		url: '/project/false',
		headers: {
			'Content-Type': 'application/json'
		}
	};

	context('project', () => {
		it('- post', async () => {
			let response = await server.inject(options0);
			response.should.be.an('object').and.contain.keys('statusCode');
			response.statusCode.should.equal(200);
		});

		it('- get', async () => {
			let response = await server.inject(options1);
			response.should.be.an('object').and.contain.keys('statusCode','payload');
			response.statusCode.should.equal(200);
			response.payload.should.be.a('string');
			let payload = JSON.parse(response.payload);
			payload.should.be.an('object').and.contain.keys('title', 'description');
			payload.title.should.equal('First');
		});
	});
});
