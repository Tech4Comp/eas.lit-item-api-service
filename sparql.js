'use strict';

const { post } = require('got'),
	{ sparqlEndpoint, dataGraphName, counterGraphName, counterName } = require('./config'),
	{ Generator } = require('sparqljs'),
	{ parseRDF, writeRDF } = require('./common'),
	// { validate } = require('./shacl-validation/shacl'),
	{ quadToStringQuad } = require('rdf-string'),
	{ isEmpty } = require('lodash'),
	AwaitLock = require('await-lock').default;

let lock = new AwaitLock();

module.exports = {

	create: async function(quads, newElement = true, graphName = dataGraphName, skipValidation = false) {//eslint-disable-line no-unused-vars
		if(!(await isExisting.bind(this)(quads))) {
			// TODO check for existence before writing new data
			// let turtle = await writeRDF(quads, 'text/turtle'); //NOTE subject is not rewritten yet
			// console.log(turtle);

			// if(!skipValidation)
			// await validate(turtle); //NOTE subject is not rewritten yet

			let newID = undefined;
			let subject = undefined;
			if(newElement)
				newID = await this.getNewID();

			let triples = quads.map((quad) => {
				let triple = quadToStringQuad(quad);
				if(newElement && !isBlankNode(triple.subject))
					triple.subject += '/' + newID;
				if(!isBlankNode(triple.subject))
					subject = triple.subject;
				delete triple.graph;
				return triple;
			});

			const query = {
				type: 'update',
				updates: [ {
					updateType: 'insert',
					insert: [{
						type: 'graph',
						triples: triples,
						name: graphName
					}]
				}]
			};

			const stringQuery = createSparqlQueryString(query);

			await post(sparqlEndpoint, {
				headers: {'Content-Type': 'application/sparql-update'},
				body: stringQuery,
				// auth: {
				// 	user: 'username',
				// 	pass: 'password',
				// 	sendImmediately: false
				// }
			});

			return subject;
		} else {
			throw 'Overriding already existing data without deleting it first';
		}
	},

	write: async function(quads, graphName = dataGraphName, skipValidation = false) {
		if(await isExisting.bind(this)(quads))
			await deleteExistingData.bind(this)(quads);
		return this.create(quads, false, graphName, skipValidation);
	},

	delete: async function(subject, graphName = dataGraphName) {

		const query = `DELETE WHERE {
			GRAPH <${graphName}> { <${subject}> ?p ?o . }
		}`;

		return post(sparqlEndpoint, {
			headers: {'Content-Type': 'application/sparql-update', 'Accept': 'application/json'},
			body: query
		});
	},

	read: async function(subject, customQuery = undefined, mimeType = 'application/n-triples', graphName = dataGraphName) {
		//NOTE this function is partially copied to shacl.js
		const query = (customQuery) ? customQuery : `
			prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			prefix eal: <http://tech4comp/eal/>

			CONSTRUCT {
				<${subject}> ?p ?o .
				?o2 ?p2 ?o3 .
			 }
			WHERE {
			 GRAPH <${graphName}> {
				 <${subject}> ?p ?o .
				 optional {
					 <${subject}> eal:hasBloomMapping ?o2 .
					 ?o2 ?p2 ?o3 .
				 }
			 }
			}`;

		let response = await post(sparqlEndpoint, {
			headers: {'Content-Type': 'application/sparql-query', 'Accept': mimeType},
			body: query
		});
		response = response.body;

		response = (mimeType === 'application/ld+json') ? JSON.parse(response) : response;

		if(mimeType !== 'application/ld+json' && (await parseRDF(response)).length === 0)
			throw 'response is empty';
		else if(mimeType === 'application/ld+json' && Object.keys(response).length === 1)
			throw 'response is empty';

		return response;
	},

	getNewID: async function (increment = true, name = counterName) {
		await lock.acquireAsync();
		try {
			const query = `prefix eal: <http://tech4comp/eal/>
				SELECT ?value
				WHERE {
					GRAPH <${counterGraphName}> { <${name}> <eal:count> ?value }
				}`;
			let response = await post(sparqlEndpoint, {
				headers: {'Content-Type': 'application/sparql-query', 'Accept': 'application/json'},
				body: query
			});
			response = response.body;
			if(increment) {
				const query2 = `prefix eal: <http://tech4comp/eal/>
					DELETE {
						GRAPH <${counterGraphName}> { <${name}> <eal:count> ?old }
					}
					INSERT {
						GRAPH <${counterGraphName}> { <${name}> <eal:count> ?new }
					}
					WHERE {
						GRAPH <${counterGraphName}> { <${name}> <eal:count> ?old }
						bind(?old+1 as ?new) .
					}`;
				await post(sparqlEndpoint, {
					headers: {'Content-Type': 'application/sparql-update', 'Accept': 'application/json'},
					body: query2
				});
			}
			const result = JSON.parse(response).results.bindings;
			if(isEmpty(result))
				return false;
			else
				return result[0].value.value;
		} catch (e) {
			console.error(e);
			return false;
		} finally {
			lock.release();
		}
	},

	createCounters: async function (name = counterName) {
		try {
			if ( (await this.getNewID(false)) !== false ) // NOTE skip if counter already exists
				return true;

			const query = `INSERT DATA {
				GRAPH <${counterGraphName}> { <${name}> <eal:count> 1 }
			}`;

			await post(sparqlEndpoint, {
				headers: {'Content-Type': 'application/sparql-update', 'Accept': 'application/n-triples'},
				body: query
			});

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	},
};

function extractSubjects(quads) {
	const subjects = quads.map((quad) => {
		const triple = quadToStringQuad(quad);
		if(!isBlankNode(triple.subject))
			return triple.subject;
		else
			return undefined;
	});
	return new Set(subjects.filter((subject) => subject !== undefined));
}

function deleteExistingData(quads) { //TODO how to delete orphaned blank nodes
	const toDelete = extractSubjects(quads);
	const existingsSubjects = Array.from(toDelete).map((subject) => this.delete(subject));
	return Promise.all(existingsSubjects);
}

function isExisting (quads) {
	const subjects = extractSubjects(quads);
	const existingsSubjects = Array.from(subjects).map((subject) => this.read(subject));
	// Exits if one subject does not exist
	return Promise.all(existingsSubjects).then(() => true).catch(() => false);
}

function createSparqlQueryString(query) {
	const generator = new Generator();
	return generator.stringify(query);
}

function isBlankNode (subject) {
	return subject.startsWith('_:');
}
