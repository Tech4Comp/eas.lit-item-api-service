'use strict';

const { isEmpty } = require('lodash');

const domain = (!isEmpty(process.env.VIRTUAL_HOST)) ? process.env.VIRTUAL_HOST : 'localhost';
const port = (!isEmpty(process.env.VIRTUAL_PORT)) ? process.env.VIRTUAL_PORT : '3000';
const protocol = 'http';
const hostname = protocol + '://' + domain + ((port === '80') ? '' : ':' + port);
console.info('Using ' + hostname + ' as hostname');

const sparqlEndpoint = (!isEmpty(process.env.SERVICE_URL_SPARQL)) ? process.env.SERVICE_URL_SPARQL : 'http://localhost:8080/eal';
console.info('Using ' + sparqlEndpoint + ' as sparql endpoint');

const dataGraphName = (!isEmpty(process.env.DATA_GRAPH_NAME)) ? process.env.DATA_GRAPH_NAME : hostname + '/data';
console.info('Using ' + dataGraphName + ' as graph name for data');

const shapesGraphName = (!isEmpty(process.env.SHAPES_GRAPH_NAME)) ? process.env.SHAPES_GRAPH_NAME : hostname + '/shapes';
console.info('Using ' + shapesGraphName + ' as graph name for shacl shapes');

const conceptsGraphName = (!isEmpty(process.env.CONCEPTS_GRAPH_NAME)) ? process.env.CONCEPTS_GRAPH_NAME : hostname + '/concepts';
console.info('Using ' + conceptsGraphName + ' as graph name for concept and property definitions');

const counterGraphName = (!isEmpty(process.env.COUNTERS_GRAPH_NAME)) ? process.env.COUNTERS_GRAPH_NAME : hostname + '/counters';
console.info('Using ' + counterGraphName + ' as graph name for counters');

const counterName = hostname + '/counter';

module.exports = {
	domain: domain,
	port: port,
	protocol: protocol,
	hostname: hostname,
	sparqlEndpoint: sparqlEndpoint,
	dataGraphName: dataGraphName,
	shapesGraphName: shapesGraphName,
	conceptsGraphName: conceptsGraphName,
	counterGraphName: counterGraphName,
	counterName: counterName
};
