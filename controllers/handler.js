'use strict';

const { badImplementation, notFound } = require('@hapi/boom'),
	sparql = require('../sparql'),
	{ negotiateRead, negotiateWrite } = require('./content-negotiation'),
	{ dataGraphName } = require('../config'),
	{ rewriteSubject } = require('../common'),
	{ isEmpty, head } = require('lodash');

let onyxHashMap;

try {
	onyxHashMap = new Map(require('../onyxHashFile.json').map((el) => [el.sha224_hash,el.id]));
} catch (e) {
	console.warn('No Onyx Hash Map provided, using empty one');
	onyxHashMap = new Map();
}

module.exports = {
	get: async function(request, h) {
		if(request.params.id === '*') {
			const targetClass = request.route.settings.tags.find((item) => item.startsWith('a ')).replace('a ','');
			const customQuery = `prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				prefix eal: <http://tech4comp/eal/>

				CONSTRUCT {
					?s a ${targetClass} .
					?s <http://purl.org/dc/terms/title> ?o .
				} WHERE {
				 GRAPH <${dataGraphName}> {
					?s a ${targetClass} .
					${!isEmpty(request.query.project) ? `?s eal:hasProject <${request.query.project}> .`: ''}
					${request.route.path.includes('/topic/') ? '?s <http://purl.org/dc/terms/title> ?o .': ''}
				 }
			 }`;
			return await negotiateRead(request, h, customQuery);
		} else
			return await negotiateRead(request, h);
	},

	getSCMCItem: async function(request, h) {
		let customQuery;
		const targetClasses = request.route.settings.tags.filter((item) => item.startsWith('a ')).map((item) => item.replace('a ',''));
		if(request.params.id === '*') {
			if(request.query.detailed) {
				customQuery = `prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
					prefix eal: <http://tech4comp/eal/>
					prefix dct: <http://purl.org/dc/terms/>
					prefix schema: <http://schema.org/>

					CONSTRUCT {
					  ?s ?p ?o6 .
						?listRest rdf:first ?head ;
							rdf:rest ?tail .
						?head ?p4 ?o5 .

						?annotations ?p2 ?o2 .
						?o3 ?p3 ?o4 .
					} WHERE {
			  		GRAPH <${dataGraphName}> {
							?s a ?o .
 						 	${!isEmpty(request.query.project) ? `?s eal:hasProject <${request.query.project}> .`: ''}
 						 	FILTER (?o IN ( ${targetClasses.join(', ')} ) )
			  		 	?s ?p ?o6 .

							optional {
								?s eal:annotation ?annotations .
								?annotations ?p2 ?o2 .
								optional {
									?annotations eal:hasBloomMapping ?o3 .
									?o3 ?p3 ?o4 .
								}
							}
							optional {
								?s eal:hasAnswer ?list .

								?list rdf:rest* ?listRest .
								?listRest rdf:first ?head ;
									rdf:rest ?tail .
								?head ?p4 ?o5 .
							}
						}
					}`;
			} else {
				customQuery = `prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
					prefix eal: <http://tech4comp/eal/>

					CONSTRUCT {
						?s a ?o .
					} WHERE {
					 GRAPH <${dataGraphName}> {
						 ?s a ?o .
						 ${!isEmpty(request.query.project) ? `?s eal:hasProject <${request.query.project}> .`: ''}
						 FILTER (?o IN ( ${targetClasses.join(', ')} ) )
					 }
				 }`;
			}
		} else if(request.params.id.startsWith('http') && targetClasses.length === 1 && head(targetClasses) === '<http://tech4comp/eal/RemoteItem>' ) {
			// TODO optimize query
			customQuery = `prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				prefix eal: <http://tech4comp/eal/>

				CONSTRUCT {
					?s a ?o .
					?s eal:remoteItemURL ?o2 .
				} WHERE {
					GRAPH <${dataGraphName}> {
						?s a ${head(targetClasses)} .
						?s eal:remoteItemURL "${request.params.id}" .
						?s a ?o .
						?s eal:remoteItemURL ?o2 .
					}
				}`;
		} else {
			const subject = rewriteSubject(request, request.url.href);
			customQuery = `prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				prefix eal: <http://tech4comp/eal/>
				prefix dct: <http://purl.org/dc/terms/>
				prefix schema: <http://schema.org/>

				CONSTRUCT {
				  <${subject}> ?p ?o .
					?listRest rdf:first ?head ;
						rdf:rest ?tail .
					?head ?p4 ?o5 .

					?annotations ?p2 ?o2 .
					?o3 ?p3 ?o4 .
				} WHERE {
		  		GRAPH <${dataGraphName}> {
		  			<${subject}> ?p ?o .

						optional {
							<${subject}> eal:annotation ?annotations .
							?annotations ?p2 ?o2 .
							optional {
								?annotations eal:hasBloomMapping ?o3 .
								?o3 ?p3 ?o4 .
							}
						}
						optional {
							<${subject}> eal:hasAnswer ?list .

							?list rdf:rest* ?listRest .
							?listRest rdf:first ?head ;
								rdf:rest ?tail .
							?head ?p4 ?o5 .
						}
					}
				}`;
		}
		return await negotiateRead(request, h, customQuery);
	},

	getDefaultData: async function(request, h) {

		if(request.params.name === '*') {
			const targetClass = request.route.settings.tags.find((item) => item.startsWith('a ')).replace('a ','');
			const customQuery = `prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				prefix eal: <http://tech4comp/eal/>

				CONSTRUCT {
					?s a ${targetClass} .
				} WHERE {
				 GRAPH <${dataGraphName}> {
					?s a ${targetClass} .
				 }
			 }`;
			return await negotiateRead(request, h, customQuery, undefined, true);
		} else {
			const subject = 'http://tech4comp/eal/' + request.params.name;
			return await negotiateRead(request, h, undefined, subject, true);
		}
	},

	delete: async function(request) {
		try {
			return await sparql.delete(request.url.href);
		} catch(error) {
			console.error(error);
			request.log('error', error);
			return badImplementation();
		}
	},

	create: async function(request, h) {
		return await negotiateWrite(request, h, true);
	},

	update: async function(request, h) {
		return await negotiateWrite(request, h);
	},

	getOnyxID: async function(request) {
		if(!isEmpty(request.params.hash)) {
			const id = onyxHashMap.get(request.params.hash);
			if(id !== undefined)
				return {id: id};
			else
				return notFound();
		}
	},
};
