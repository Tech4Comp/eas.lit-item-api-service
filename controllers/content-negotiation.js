'use strict';

const boom = require('@hapi/boom'),
	sparql = require('../sparql'),
	{ IsValidJson, isTurtle, parseRDF, writeRDF, rewriteSubject } = require('../common'),
	jsonldConverter = require('../json-processing/json-to-jsonld'),
	{ isEmpty } = require('lodash'),
	{ DataFactory } = require('n3'),
	jsonld = require('jsonld');

const ealNS = 'http://tech4comp/eal/';

module.exports = {
	negotiateWrite: async function(request, h, createNew = false) {
		try {
			let data = request.payload;
			let tmp = undefined;

			const targetClass = request.route.settings.tags.find((item) => item.startsWith('a ')).replace('a ','');
			const subject = rewriteSubject(request);

			switch (request.headers['content-type']) {
				case 'application/json':
					if(IsValidJson(data)) {
						data = await jsonldConverter.addContext(data, subject, targetClass);
					} else
						return boom.badData('Data is not valid JSON data');
				case 'application/ld+json':
					if(IsValidJson(data)) {
						if(isEmpty(data['@context']))
							return boom.badData('Data is not valid JSON-LD data');
						else {
							data = addTimestamps(data);
							tmp = await jsonldConverter.jsonldToQuadStrings(data);
							const url = (createNew) ? await sparql.create(tmp) : await sparql.write(tmp);
							return internalGet(request, h, url);
						}
					} else
						return boom.badData('Data is not valid JSON data');
				case 'text/turtle'://other formats would be implemented analogous
					if(await isTurtle(data)){
						tmp = await parseRDF(data);
						if(tmp.some((quad) => !(quad.subject.id === request.url.href || quad.subject.id.startsWith('_:'))))
							return boom.badData('Subject does not match route');
						const url = (createNew) ? await sparql.create(tmp) : await sparql.write(tmp);
						return internalGet(request, h, url);
					} else
						return boom.badData('Data is not valid turtle data, please check with RaptorUtils');
				default:
					return boom.unsupportedMediaType();
			}
		} catch (error) {
			if (error instanceof Object && error['@type'] === 'ValidationReport')
				return boom.badData('Bad data, see report' , error);
			console.error(error);
			request.log('error', error);
			return boom.badImplementation();
		}
	},

	negotiateRead: async function(request, h, customQuery = undefined, subject = undefined, addSameAs = false) {
		try {
			subject = rewriteSubject(request, subject);
			let rdfFormat = (request.headers.accept === 'text/turtle') ? 'text/turtle' : 'application/n-triples';
			// rdfFormat = (request.headers.accept === 'application/ld+json' || request.headers.accept === 'application/json') ? 'application/ld+json' : rdfFormat;
			let toRespond;
			try {
				let time1 = new Date().getTime();
				toRespond = await sparql.read(subject, customQuery, rdfFormat);
				let time2 = new Date().getTime();
				console.log('sparql: ' + (time2 - time1));
			} catch (e) {
				return boom.notFound();
			}
			if(addSameAs)
				toRespond = await addSameAsStatements(toRespond, request.url.href, rdfFormat);

			let targetClass = request.route.settings.tags.find((item) => item.startsWith('a '));
			if(isEmpty(targetClass)) {
				if(subject.includes('/item'))
					targetClass = '<' + ealNS + 'Item' + '>';
			} else
				targetClass = targetClass.replace('a ','');

			const accepts = request.headers.accept.split(',');
			const acceptAll = accepts.find((accept) => accept.includes('*/*'));
			if(!isEmpty(acceptAll))
				accepts[0] = 'application/json';
			switch (accepts[0]) {
				case 'application/json':
				case 'application/ld+json':
					toRespond = await jsonldConverter.ntriplesToJSONLD(toRespond, targetClass);
					// toRespond = await jsonldConverter.replaceJSONLDContext(toRespond, targetClass);
					return h.response(toRespond).type('application/ld+json');
				case 'text/turtle'://other formats would be implemented analogous
					return h.response(toRespond).type('text/turtle');
				default:
					return boom.unsupportedMediaType();
			}
		} catch(error) {
			console.error(error);
			request.log('error', error);
			return boom.badImplementation();
		}
	},
};

async function internalGet(request, h, url) {
	const response = await global.server.inject({url: url, headers: request.headers});
	return h.response(response.result).code(response.statusCode).type(response.headers['content-type']);
}

async function addSameAsStatements(data, url, format) {
	const { namedNode, defaultGraph, quad } = DataFactory;
	const pre = url.slice(0,url.lastIndexOf('/') + 1);
	if(format === 'application/ld+json' || format === 'application/json')
		data = await jsonld.toRDF(data, {format: 'application/n-quads'});
	let quads = await parseRDF(data);
	let distinctSubjects = new Set();
	quads.forEach((quad) => distinctSubjects.add(quad.subject.value));
	distinctSubjects.forEach((subject) => {
		let app = subject.slice(subject.lastIndexOf('/') + 1, subject.length);
		const myQuad = quad(
			namedNode(pre+app),
			namedNode('http://www.w3.org/2002/07/owl#sameAs'),
			namedNode(subject),
			defaultGraph(),
		);
		quads.push(myQuad);
	});
	if(format === 'application/ld+json' || format === 'application/json')
		return await jsonld.fromRDF(await await writeRDF(quads), {format: 'application/n-quads'});
	else
		return await writeRDF(quads, format);
}

function addTimestamps (data /*as jsonLD*/) {
	data.modificationTime = new Date().toISOString();
	if(data.creationTime === undefined)
		data.creationTime = new Date().toISOString();
	else
		data.creationTime = new Date(data.creationTime).toISOString();
	return data;
}
